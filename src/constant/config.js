const apiURL = 'http://localhost:5000/'

const apiHeader = {
    'Content-Type': 'application/json',
    // 'Authorization': 'Bearer' + window.localStorage.getItem('token'),
    'Authorization': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJuYW1lIjoieGlvbmciLCJwZXJtaXNzaW9uIjoiYWRtaW4ifSwiaWF0IjoxNjYzNjY1NTcxLCJleHAiOjE2NjQwOTc1NzF9.jqmRV4SMXjk2e8SLH2RmtpffpzyAAkxWF0OmYynDPII',

}

export {
    apiURL,
    apiHeader
}