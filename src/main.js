import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify';
import VueToast from 'vue-toast-notification'
import 'vue-toast-notification/dist/theme-sugar.css';

Vue.config.productionTip = false

Vue.use(VueToast);
let instance = Vue.$toast.open('You did it!');
instance.dismiss();
Vue.$toast.clear();

new Vue({
    router,
    store,
    vuetify,
    VueToast,
    render: h => h(App)
}).$mount('#app')