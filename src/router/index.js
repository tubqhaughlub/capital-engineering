import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../components/Dashbord.vue'

import Documents from '../components/Manage/Documents/Document-Index.vue'
import DocumentIn from '../components/Manage/Documents/Document-In.vue'
import DocumentOut from '../components/Manage/Documents/Document-Out.vue'

import Materials from '../components/Manage/Materials/Material-Index.vue'
import MaterialName from '../components/Manage/Materials/Material-Name.vue'
import MaterialCategory from '../components/Manage/Materials/Material-Category.vue'

import DepartmentIndex from '../components/Manage/Departments/Department-Index.vue'
import Users from '../components/Manage/Users/User-Index'
import Employees from '../components/Manage/Employees/Employee-Index.vue'
import EmployeesView from '../components/Manage/Employees/Employee-View.vue'

import Analytics from '../components/Analytics.vue'
import Revenue from '../components/Revenue.vue'

import Order from '../components/Carts/Order/Order-Index.vue'
import OrderAdd from '../components/Carts/Order/Order-Add.vue'
import Import from '../components/Carts/Import/Import-Index.vue'
import ImportAdd from '../components/Carts/Import/Import-Add.vue'

import emp from '../components/Manage/Employees/Employee-Add'
import edit from '../components/Manage/Employees/Employee-Edit'
import UsersAdd from '../components/Manage/Users/User-Add'
import UsersEdit from '../components/Manage/Users/User-Edit'
import SupplierIndex from '../components/Manage/Supplier/Supplier-Index'

import Login from '../views/Login'


Vue.use(VueRouter)


const routes = [

    {
        path: '/login',
        name: 'login',
        component: Login
    },

    {
        path: '/',
        name: 'Home',
        component: Home
    },
    {
        path: '/about',
        name: 'About',
        component: () =>
            import ('../views/About.vue')
    },

    {
        path: '/revenue',
        name: 'Revenue',
        component: Revenue
    },

    {
        path: '/chart',
        name: 'Analytics',
        component: Analytics
    },
    {
        path: '/user-index',
        name: 'Users',
        component: Users
    },

    // Manage-Documents Proccess
    {
        path: '/manage/documents/documents-index',
        name: 'Documents',
        component: Documents
    },
    {
        path: '/manage/documents/document-in',
        name: 'DocumentIn',
        component: DocumentIn
    },
    {
        path: '/manage/documents/document-out',
        name: 'DocumentOut',
        component: DocumentOut
    },
    // Manage-Materials Proccess
    {
        path: '/manage/materials/material-index',
        name: 'Materials',
        component: Materials
    },
    {
        path: '/manage/materials/material-name',
        name: 'MaterialName',
        component: MaterialName
    },
    {
        path: '/manage/materials/material-category',
        name: 'MaterialCategory',
        component: MaterialCategory
    },
    // Order Proccess
    {
        path: '/order',
        name: 'Order',
        component: Order
    },
    {
        path: '/order-add',
        name: 'OrderAdd',
        component: OrderAdd
    },
    //Import Proccess
    {
        path: '/import',
        name: 'Import',
        component: Import
    },
    {
        path: '/importAdd',
        name: 'ImportAdd',
        component: ImportAdd
    },
    //Employees Proccess
    {
        path: '/manage/employees/employee-index',
        name: 'Employees',
        component: Employees
    },
    {
        path: '/employee/add',
        name: 'Add',
        component: emp
    },
    {
        path: '/employee/edit',
        name: 'edit',
        component: edit
    },
    {
        path: '/employee/view',
        name: 'EmployeesView',
        component: EmployeesView
    },
    //Users Proccess
    {
        path: '/users/add',
        name: 'Add',
        component: UsersAdd
    },
    {
        path: '/users/edit',
        name: 'Edit',
        component: UsersEdit
    },
    //Supplier Proccess
    {
        path: '/supplier',
        name: 'SupplierIndex',
        component: SupplierIndex
    },
    //Department Process
    {
        path: '/department-index',
        name: 'DepartmentIndex',
        component: DepartmentIndex
    }
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router