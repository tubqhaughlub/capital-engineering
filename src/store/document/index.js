import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";
import moment from "moment/moment";
import { apiHeader, apiURL } from "@/constant/config";

Vue.use(Vuex);
const documentModules = {
    namespaced: true,

    state: {
        documents: [],
        document: [],
        length: true,
        loading: false,
    },
    actions: {
        async getDocuments({ commit }) {
            try {
                commit("setLoading", true);
                await axios
                    .get(apiURL + `documents`, { headers: apiHeader })
                    .then((res) => {
                        const list = [];
                        for (let i in res.data) {
                            const el = res.data[i];
                            const obj = {
                                idx: parseInt(i) + 1,
                                id: el.document_id,
                                name: el.doc_name,
                                description: el.description,
                                type: el.type,
                                image: null,
                                imageUrl: el.image,
                                employee_id: el.employee_id,
                                employee_fname: el.employee_fname,
                                employee_lname: el.employee_lname,
                                created_date: moment(el.create_date).format("dd-MM-yyyy"),
                            };
                            list.push(obj);
                        }
                        commit("setDocuments", list);
                        commit("setLength", true);
                    })
                    .catch((error) => {
                        console.log(error);
                        commit("setLength", false);
                    });
            } catch (error) {
                console.log(error);
            }
        },
        async getDocument({ commit }, id) {
            try {
                commit("setLoading", true);
                await axios
                    .get(apiURL + `document/${id}`, { headers: apiHeader })
                    .then((res) => {
                        const el = res.data;
                        const obj = {
                            id: el.document_id,
                            name: el.doc_name,
                            description: el.description,
                            type: el.type,
                            image: el.image,
                            employee_id: el.employee_id,
                            employee_fname: el.employee_fname,
                            employee_lname: el.employee_lname,
                            created_date: moment(el.create_date).format("dd-MM-yyyy"),
                        };
                        commit("setDocument", obj);
                    })
                    .catch((error) => {
                        console.log(error);
                    });
            } catch (error) {
                console.log(error);
            }
        },
        postDocument({ commit }, item) {
            try {
                commit("setLoading", true);
                return axios.post(apiURL + `create/document`, item, {
                    headers: apiHeader,
                });
            } catch (error) {
                console.log(error);
            }
        },
        putDocument({ commit }, item) {
            try {
                // alert(String(item))
                commit("setLoading", true);
                return axios.put(apiURL + `document/${item.id}`, item, {
                    headers: apiHeader,
                });
            } catch (error) {
                console.log(error);
            }
        },
        delDocument({ commit }, item) {
            try {
                commit("setLoading", true);
                return axios.delete(apiURL + `document/delete/${item.id}`, {
                    headers: apiHeader,
                });
            } catch (error) {
                console.log(error);
            }
        },
    },
    mutations: {
        setDocuments: (state, data) => (state.documents = data),
        setDocument: (state, data) => (state.document = data),
        setLength: (state, length) => (state.length = length),
        setLoading: (state, type) => (state.loading = type),
    },
    getters: {
        documents: (state) => state.documents,
        document: (state) => state.document,
        length: (state) => state.length,
        loading: (state) => state.loading,
    },
};

export default documentModules;