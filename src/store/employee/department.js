import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";
import moment from "moment/moment";
import { apiHeader, apiURL } from "@/constant/config";

Vue.use(Vuex);
const departmentModules = {
    namespaced: true,

    state: {
        departments: [],
        department: [],
        length: [],
        loading: [],
    },

    actions: {
        async getDepartments({ commit }) {
            try {
                commit('setLoading', true)
                await axios.get(apiURL + `departments`, { headers: apiHeader })
                    .then(res => {
                        const list = []
                        for (let i in res.data) {
                            const el = res.data[i];
                            const obj = {
                                idx: parseInt(i) + 1,
                                id: el.department_id,
                                department_name: el.department_name,
                                create_date: moment(el.create_date).format('dd-MM-yyyy')
                            }
                            list.push(obj);
                        }
                        commit('setDepartments', list);
                        commit('setLength', true)
                    })
                    .catch(error => {
                        commit('setLength', false);
                        console.log(error);
                    })
            }
            catch (error) {
                console.log(error);
            }
        },
        async getDepartment({ commit }, id) {
            try {
                commit('setLoading', true)
                await axios.get(apiURL + `department/${id}`, { headers: apiHeader })
                    .then(res => {
                        const el = res.data;
                        const obj = {
                            department_id: el.department_id,
                            department_name: el.department_name,
                            create_date: moment(el.create_date).format('dd-MM-yyyy')
                        }
                        commit('setDepartment', obj)
                    })
                    .catch(error => {
                        console.log(error);
                    });
            }
            catch (error) {
                console.log(error);
            }
        },
        postDepartment({ commit }, item) {
            try {
                commit('setLoading', true)
                return axios.post(apiURL + `create-department`, item, { headers: apiHeader })
            }
            catch (error) {
                console.log(error);
            }
        },
        putDepartment({ commit }, item) {
            try {
                // alert(JSON.stringify(item.id));
                // alert(item);
                commit('setLoading', true)
                return axios.put(apiURL + `department/update/${item.id}`, item, { headers: apiHeader })
            }
            catch (error) {
                console.log(error);
            }
        },
        delDepartment({ commit }, item) {
            try {
                // alert(JSON.stringify(item));
                commit('setLoading', true)
                return axios.delete(apiURL + `department/delete/${item.id}`, item, { headers: apiHeader })
            }
            catch (error) {
                console.log(error);
            }
        }
    },
    mutations: {
        setDepartments: (state, data) => state.departments = data,
        setDepartmentid: (state, data) => state.departmentid = data,
        setLength: (state, length) => state.length = length,
        setLoading: (state, type) => state.loading = type,
    },
    getters: {
        departments: (state) => state.departments,
        department: (state) => state.department,
        length: (state) => state.length,
        loading: (state) => state.loading,
    }
}

export default departmentModules;