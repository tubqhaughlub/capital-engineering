import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";
import moment from "moment/moment";
import { apiHeader, apiURL } from "../../constant/config";

Vue.use(Vuex);
const employeeModules = {
    namespaced: true,

    state: {
        employees: [],
        employee: [],
        length: [],
        loading: [],
        firstname: [],
    },

    actions: {
        async getEmployees({ commit }) {
            try {

                commit('setLoading', true)
                await axios.get(apiURL + `employees`, { headers: apiHeader })
                    .then(res => {
                        const list = [], name = []
                        for (let i in res.data) {
                            const el = res.data[i];

                            const obj = {
                                idx: parseInt(i) + 1,
                                id: el.emp_id,
                                firstname: el.firstname,
                                lastname: el.lastname,
                                tel: el.tel,
                                email: el.email,
                                saraly: el.saraly,
                                image: el.image,
                                department: el.department,
                                permission: el.permission,
                                create_date: moment(el.create_date).format('dd-MM-yyyy')
                            }
                            list.push(obj)
                            name.push(el.firstname)
                        }
                        commit('setEmployees', list);
                        commit('setLength', true)
                        commit('setEmpName', name)
                    })
                    .catch(error => {
                        commit('setLength', false);
                        console.log(error);
                    })
            }
            catch (error) {
                console.log(error);
            }
        },
        async getEmployee({ commit }, id) {
            try {
                commit('setLoading', true)
                await axios.get(apiURL + `employees/${id}`, { headers: apiHeader })
                    .then(res => {
                        const el = res.data;
                        const obj = {
                            id: el.emp_id,
                            firstname: el.firstname,
                            lastname: el.lastname,
                            tel: el.tel,
                            email: el.email,
                            saraly: el.saraly,
                            image: el.image,
                            department: el.department,
                            permission: el.permission,
                            create_date: moment(el.create_date).format('dd-MM-yyyy')
                        }
                        commit('setEmployee', obj)
                    })
                    .catch(error => {
                        // commit('setLength', false);
                        console.log(error);
                    })
            }
            catch (error) {
                console.log(error);
            }
        },
        postEmployee({ commit }, item) {
            try {
                commit('setLoading', true)
                return axios.post(apiURL + `employees`, item, { headers: apiHeader })
            }
            catch (error) {
                console.log(error);
            }
        },
        putEmployee({ commit }, item) {
            try {
                commit('setLoading', true)
                return axios.put(apiURL + `employees/${item.id}`, item, { headers: apiHeader })
            }
            catch (error) {
                console.log(error);
            }
        },
        delEmployee({ commit }, item) {
            try {
                commit('setLoading', true)
                return axios.delete(apiURL + `employees/${item.id}`, item, { headers: apiHeader })
            }
            catch (error) {
                console.log(error);
            }
        }
    },
    mutations: {
        setEmployees: (state, data) => state.employees = data,
        setEmployee: (state, data) => state.employee = data,
        setEmpName: (state, data) => state.firstname = data,
        setLength: (state, length) => state.length = length,
        setLoading: (state, type) => state.loading = type,
    },
    getters: {
        employees: (state) => state.employees,
        employee: (state) => state.employee,
        firstname: (state) => state.firstname,
        length: (state) => state.length,
        loading: (state) => state.loading,
    }
}
export default employeeModules;
