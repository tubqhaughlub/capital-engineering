import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
// import moment from 'moment/moment'
import { apiHeader, apiURL } from '../../constant/config.js'


Vue.use(Vuex)
const userModules = {
    namespaced: true,

    state: {
        users: [],
        user: [],
        length: true,
        loading: false
    },

    actions: {
        async getUsers({ commit }) {
            try {
                commit('setLoading', true);
                await axios.get(apiURL + `users/name`, { headers: apiHeader })
                    .then(res => {
                        let list = []
                        for (let i in res.data) {
                            const el = res.data[i];
                            const obj = {
                                idx: parseInt(i) + 1,
                                id: el.employee_id,
                                employee_fname: el.employee_fname,
                                employee_lname: el.employee_lname,
                                employee_tel: el.employee_tel,
                                department_name: el.department_name,
                                permission: el.permission
                            }
                            list.push(obj)
                        }
                        commit('setUsers', list);
                        commit('setLength', true);
                    })
                    .catch(e => {
                        commit('setLength', false);
                        console.log(e);
                    })
            }
            catch (error) {
                console.log(error);
            }
        },

        async getUser({ commit }, id) {
            try {
                commit('setLoading', true)
                await axios.get(apiURL + `user/${id}`, { headers: apiHeader })
                    .then(res => {
                        const el = res.data;
                        const obj = {
                            id: el.employee_id,
                            employee_fname: el.employee_fname,
                            employee_lname: el.employee_lname,
                            employee_tel: el.employee_tel,
                            department_name: el.department_name,
                            permission: el.permission
                        }
                        commit('setUser', obj)
                    })
                    .catch(e => {
                        console.log(e);
                    })
            }
            catch (error) {
                console.log(error);
            }
        },
        Login({ commit }, item) {
            try {
                commit('setLoading', true)
                return axios.post(apiURL + `login`, item, { header: apiHeader })
            }
            catch (error) {
                console.log(error);
            }
        },
        Register({ commit }, item) {
            try {
                commit('setLoading', true)
                return axios.post(apiURL + `register`, item, { header: apiHeader })
            }
            catch (error) {
                console.log(error);
            }
        },
        delUser({ commit }, item) {
            try {
                alert(String(item))
                commit('setLoading', true)
                return axios.delete(apiURL + `delete/user/${item.id}`, { header: apiHeader })
            }
            catch (error) {
                console.log(error);
            }
        },
    },
    mutations: {
        setUsers: (state, data) => state.users = data,
        setUser: (state, data) => state.user = data,
        setLength: (state, length) => state.length = length,
        setLoading: (state, type) => state.loading = type,
    },
    getters: {
        users: (state) => state.users,
        user: (state) => state.user,
        length: (state) => state.length,
        loading: (state) => state.loading
    }
}

export default userModules