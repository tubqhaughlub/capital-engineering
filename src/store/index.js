import Vue from 'vue'
import Vuex from 'vuex'
import categories from './material/category';
import materials from './material/index';
import user from './employee/user';
import department from './employee/department';
import employee from './employee/index';
import supplier from './supplier/index'
import order from './order/order.js';
import importOrder from './order/import'
import document from './document/index'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    categoriesMod: categories,
    materialsMod: materials,
    userModules: user,
    supplierMod: supplier,
    departmentModules: department,
    employeeModules: employee,
    orderModules: order,
    importOrderMod: importOrder,
    documentModules: document,
  }
})
