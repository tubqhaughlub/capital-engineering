import Vue from 'vue';
import Vuex from 'vuex'
import axios from 'axios';
import moment from 'moment/moment';
import { apiURL, apiHeader } from '@/constant/config'

Vue.use(Vuex);
const categoryModules = {
    namespaced: true,

    state: {
        categories: [],
        category: [],
        length: true,
        loading: false,
        category_name: [],
    },
    actions: {
        async getCategories({ commit }) {
            try {
                commit('setLoading', true)
                await axios.get(apiURL + `categorys`, {
                    headers: apiHeader
                })
                    .then(res => {
                        const list = [], name = []
                        for (let i in res.data) {
                            const el = res.data[i];

                            const obj = {
                                idx: parseInt(i) + 1,
                                id: el.category_id,
                                category_name: el.category_name,
                                createdAt: moment(el.create_date).format('dd-MM-yyyy')
                            }
                            list.push(obj);
                            name.push(el.category_name)
                        }
                        commit('setCategories', list)
                        commit('setLength', true);
                        commit('setCateName', name)

                    }).catch(e => {
                        commit('setLength', false);
                        console.log(e);
                    })

            } catch (error) {
                console.log(error);
            }
        },
        async getCategory({ commit }, id) {
            try {
                commit('setLoading', true)
                await axios.get(apiURL + `category/${id}`, {
                    headers: apiHeader
                })
                    .then(res => {
                        const el = res.data;
                        const obj = {
                            id: el.category_id,
                            category_name: el.category_name,
                            createdAt: moment(el.create_date).format('dd-MM-yyyy')
                        }
                        commit('setCategory', obj)
                    }).catch(e => {
                        console.log(e);
                    });
            } catch (error) {
                console.log(error);
            }
        },
        postCategory({ commit }, item) {
            try {
                commit('setLoading', true)
                return axios.post(apiURL + `category`, item, {
                    headers: apiHeader
                })
            } catch (error) {
                console.log(error);
            }
        },
        putCategory({ commit }, item) {
            try {
                commit('setLoading', true)
                return axios.put(apiURL + `category/update/${item.id}`, item, {
                    headers: apiHeader
                })
            } catch (error) {
                console.log(error);
            }
        },
        delCategory({ commit }, item) {
            try {
                // alert(JSON.stringify(item));

                commit('setLoading', true)
                return axios.delete(apiURL + `category/delete/${item.id}`, {
                    headers: apiHeader
                })
            } catch (error) {
                console.log(error);
            }
        },
    },
    mutations: {
        setCategories: (state, data) => state.categories = data,
        setCategory: (state, data) => state.category = data,
        setLength(state, length) { state.length = length },
        setLoading: (state, type) => state.loading = type,
        setCateName: (state, dt) => state.category_name = dt
    },
    getters: {
        categories: (state) => state.categories,
        category: (state) => state.category,
        length: (state) => state.length,
        loading: (state) => state.loading,
        category_name: state => state.category_name
    }

}

export default categoryModules;