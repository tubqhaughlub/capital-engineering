import Vue from 'vue';
import Vuex from 'vuex'

Vue.use(Vuex);
import {
    apiURL,
    apiHeader
} from '@/constant/config'
import axios from 'axios';
import moment from 'moment/moment';

const materialModules = {
    namespaced: true,

    state: {
        materials: [],
        material: [],
        length: true,
        loading: false
    },
    actions: {
        async getMaterials({
            commit
        }) {
            try {
                commit('setLoading', true);
                await axios.get(apiURL + `materialbyname`, {
                    headers: apiHeader
                })
                    .then(res => {
                        let list = []
                        for (let i in res.data) {
                            const el = res.data[i];

                            const obj = {
                                idx: parseInt(i) + 1,
                                id: el.material_id,
                                name: el.material_name,
                                price: el.price,
                                amount: el.amount,
                                category_id: el.category_id,
                                category: el.category_name,
                                createdAt: moment(el.create_date).format('dd-MM-yyyy')
                            }
                            list.push(obj)
                        }
                        commit('setMaterials', list);
                        commit('setLength', true);
                    })
                    .catch(e => {
                        commit('setLength', false);
                        console.log(e);
                    })
            } catch (error) {
                console.log(error);
            }
        },
        async getMaterial({ commit }, id) {
            try {
                commit('setLoading', true)
                await axios.get(apiURL + `material/${id}`, { headers: apiHeader })
                    .then(res => {
                        const el = res.data;
                        const obj = {
                            id: el.material_id,
                            name: el.material_name,
                            price: el.material_price,
                            amount: el.material_amount,
                            createdAt: moment(el.create_date).format('dd-MM-yyyy')
                        }
                        commit('setMaterial', obj)
                    }).catch(e => {
                        console.log(e);
                    });
            } catch (error) {
                console.log(error);
            }
        },
        postMaterial({
            commit
        }, item) {
            try {
                commit('setLoading', true)
                return axios.post(apiURL + `material/create`, item, {
                    headers: apiHeader
                })
            } catch (error) {
                console.log(error);
            }
        },
        putMaterial({
            commit
        }, item) {
            try {
                // alert(JSON.stringify(item))
                commit('setLoading', true)
                return axios.put(apiURL + `material/update/${item.id}`, item, {
                    headers: apiHeader
                })
            } catch (error) {
                console.log(error);
            }
        },
        deltMaterial({
            commit
        }, item) {
            try {

                commit('setLoading', true)
                return axios.delete(apiURL + `material/delete/${item.id}`, {
                    headers: apiHeader
                })
            } catch (error) {
                console.log(error);
            }
        },
    },
    mutations: {
        setMaterials: (state, data) => state.materials = data,
        setMaterial: (state, data) => state.material = data,
        setLength: (state, length) => state.length = length,
        setLoading: (state, type) => state.loading = type,
    },
    getters: {
        materials: (state) => state.materials,
        material: (state) => state.material,
        length: (state) => state.length,
        loading: (state) => state.loading
    }
}

export default materialModules;