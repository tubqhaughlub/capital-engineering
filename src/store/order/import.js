import Vue from "vue";
import Vuex from 'vuex';
import axios from 'axios';
import moment from "moment/moment";
import { apiHeader, apiURL } from "@/constant/config";

Vue.use(Vuex)
const importOrderMod = {
    namespaced: true,

    state: {
        imports: [],
        import: [],
        length: true,
        loading: false,
    },
    actions: {
        async getImports({ commit }) {
            try {
                commit('setLoading', true)
                await axios.get(apiURL + `imports`, { headers: apiHeader })
                    .then(res => {
                        const list = []
                        for (let i in res.data) {
                            const el = res.data[i];
                            const obj = {
                                idx: parseInt(i) + 1,
                                id: el.importId,
                                order_id: el.order_id,
                                material_name: el.material_name,
                                employee: el.employee,
                                amount: el.amount,
                                price: el.price,
                                order_date: moment(el.create_date).format('dd-MM-yyyy')
                            }
                            list.push(obj)
                        }
                        commit('setImports', list);
                        commit('setLength', true)
                    })
                    .catch(error => {
                        console.log(error);
                        commit('setLength', false);
                    })
            }
            catch (error) {
                console.log(error);
            }
        },
        async getImport({ commit }, id) {
            try {
                commit('setLoading', true)
                await axios.get(apiURL + `import/${id}`, { headers: apiHeader })
                    .then(res => {
                        const el = res.data;
                        const obj = {
                            id: el.importId,
                            order_id: el.order_id,
                            material_name: el.material_name,
                            employee: el.employee,
                            amount: el.amount,
                            price: el.price,
                            order_date: moment(el.create_date).format('dd-MM-yyyy')
                        }
                        commit('setImport', obj)
                    })
                    .catch(error => {
                        console.log(error);
                    });
            }
            catch (error) {
                console.log(error);
            }
        },
        postImport({ commit }, item) {
            try {
                commit('setLoading', true)
                return axios.post(apiURL + `imports`, item, { headers: apiHeader })
            }
            catch (error) {
                console.log(error);
            }
        },
        putImport({ commit }, item) {
            try {
                commit('setLoading', true)
                return axios.put(apiURL + `import/${item.id}`, item, { headers: apiHeader })
            }
            catch (error) {
                console.log(error);
            }
        },
        delImport({ commit }, item) {
            try {
                alert(JSON.stringify(item.id))
                commit('setLoading', true)
                return axios.delete(apiURL + `import/delete/${item.id}`, { headers: apiHeader })
            }
            catch (error) {
                console.log(error);
            }
        }
    },
    mutations: {
        setImports: (state, data) => state.imports = data,
        setImport: (state, data) => state.import = data,
        setLength: (state, length) => state.length = length,
        setLoading: (state, type) => state.loading = type,
    },
    getters: {
        imports: (state) => state.imports,
        import: (state) => state.import,
        length: (state) => state.length,
        loading: (state) => state.loading,
    }
};

export default importOrderMod;