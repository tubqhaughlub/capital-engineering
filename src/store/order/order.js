import Vue from "vue";
import Vuex from 'vuex';
import axios from 'axios';
import moment from "moment/moment";
import { apiHeader, apiURL } from "@/constant/config";

Vue.use(Vuex)
const orderModules = {
    namespaced: true,

    state: {
        orders: [],
        order: [],
        length: true,
        loading: false,
    },
    actions: {
        async getOrders({ commit }) {
            try {
                commit('setLoading', true)
                await axios.get(apiURL + `orders/name`, { headers: apiHeader })
                    .then(res => {
                        const list = []
                        for (let i in res.data) {
                            const el = res.data[i];
                            const obj = {
                                idx: parseInt(i) + 1,
                                id: el.orderId,
                                employee: el.employee,
                                supplier: el.supplier,
                                material_id: el.material_id,
                                material_name: el.material_name,
                                amount: el.amount,
                                ORDER_date: moment(el.create_date).format('dd-MM-yyyy')
                            }
                            list.push(obj)
                        }
                        commit('setOrders', list);
                        commit('setLength', true)
                    })
                    .catch(error => {
                        console.log(error);
                        commit('setLength', false);
                    })
            }
            catch (error) {
                console.log(error);
            }
        },
        async getOrder({ commit }, id) {
            try {
                commit('setLoading', true)
                await axios.get(apiURL + `order/${id}`, { headers: apiHeader })
                    .then(res => {
                        const el = res.data;
                        const obj = {
                            id: el.orderId,
                            employee: el.employee,
                            supplier: el.supplier,
                            material_id: el.material_id,
                            material_name: el.material_name,
                            amount: el.amount,
                            ORDER_date: moment(el.create_date).format('dd-MM-yyyy')
                        }
                        commit('setOrder', obj)
                    })
                    .catch(error => {
                        console.log(error);
                    });
            }
            catch (error) {
                console.log(error);
            }
        },
        postOrder({ commit }, item) {
            try {
                commit('setLoading', true)
                return axios.post(apiURL + `create_orderProduct`, item, { headers: apiHeader })
            }
            catch (error) {
                console.log(error);
            }
        },
        putOrder({ commit }, item) {
            try {
                console.log(item);
                commit('setLoading', true)
                return axios.put(apiURL + `update/${item.id}`, item, { headers: apiHeader })
            }
            catch (error) {
                console.log(error);
            }
        },
        delOrder({ commit }, item) {
            try {
                commit('setLoading', true)
                return axios.delete(apiURL + `order/delete/${item.id}`, { headers: apiHeader })
            }
            catch (error) {
                console.log(error);
            }
        }
    },
    mutations: {
        setOrders: (state, data) => state.orders = data,
        setOrder: (state, data) => state.order = data,
        setLength: (state, length) => state.length = length,
        setLoading: (state, type) => state.Loading = type,
    },
    getters: {
        orders: (state) => state.orders,
        order: (state) => state.order,
        length: (state) => state.length,
        loading: (state) => state.loading,
    }
};

export default orderModules;