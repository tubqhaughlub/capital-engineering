import Vue from "vue";
import Vuex from 'vuex';
import axios from 'axios';
import moment from "moment/moment";
import { apiHeader, apiURL } from "../../constant/config";

Vue.use(Vuex)
const supplierModules = {
    namespaced: true,

    state: {
        suppliers: [],
        supplier: [],
        length: true,
        loading: false,
    },
    actions: {
        async getSuppliers({ commit }) {
            try {
                commit('setLoading', true)
                await axios.get(apiURL + `suppliers`, { headers: apiHeader })
                    .then(res => {
                        let list = []
                        for (let i in res.data) {
                            const el = res.data[i];
                            const obj = {
                                idx: parseInt(i) + 1,
                                id: el.supplier_id,
                                name: el.supplier_name,
                                address: el.supplier_address,
                                tel: el.supplier_tel,
                                create_date: moment(el.create_date).format('dd-MM-yyyy'),
                            }
                            list.push(obj)
                        }
                        commit('setSuppliers', list);
                        commit('setLength', true)
                    })
                    .catch(e => {
                        console.log(e);
                        commit('setLength', false)
                    })
            }
            catch (error) {
                console.log(error);
            }
        },
        async getSupplier({ commit }, id) {
            try {
                commit('setLoading', true)
                await axios.get(apiURL + `supplier/${id}`, { headers: apiHeader })
                    .then(res => {
                        const el = res.data;
                        const obj = {
                            id: el.supplier_id,
                            name: el.supplier_name,
                            address: el.supplier_address,
                            tel: el.supplier_tel,
                            create_data: moment(el.create_date).format('dd-MM-yyyy'),
                        }
                        commit('setSupplier', obj)
                    })
                    .catch(e => {
                        console.log(e);
                    })
            }
            catch (error) {
                console.log(error);
            }
        },
        postSupplier({ commit }, item) {
            try {
                commit('setLoading', true)
                return axios.post(apiURL + `supplier/create`, item, { headers: apiHeader })
            }
            catch (error) {
                console.log(error);
            }
        },
        putSupplier({ commit }, item) {
            try {
                // alert(JSON.stringify(item.id));
                // alert(item.id);

                commit('setLoading', true)
                return axios.put(apiURL + `supplier/update/${item.id}`, item, { headers: apiHeader })
            }
            catch (error) {
                console.log(error);
            }
        },
        delSupplier({ commit }, item) {
            try {
                commit('setLoading', true)
                return axios.delete(apiURL + `supplier/delete/${item.id}`, { headers: apiHeader })
            }
            catch (error) {
                console.log(error);
            }
        }
    },
    mutations: {
        setSuppliers: (state, data) => state.suppliers = data,
        setSupplier: (state, data) => state.supplier = data,
        setLength: (state, length) => state.length = length,
        setLoading: (state, type) => state.Loading = type,
    },
    getters: {
        suppliers: (state) => state.suppliers,
        supplier: (state) => state.supplier,
        length: (state) => state.length,
        loading: (state) => state.loading,
    }
}

export default supplierModules;